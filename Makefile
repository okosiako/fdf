NAME = fdf
CC = gcc
CFLAGS = -Wall -Wextra -Werror -O3 -g
INCLUDES = -I $(LIBFT_HEAD_FLD) -I $(HEADERS_FLD) -I $(PRINT_HEAD_FLD) -I $(MLX_FLD)
LIB_INCLUDE = -L $(LIBFT_LIB_FLD) -lft $(MLX_LIB_INC) -L $(PRINT_LIB_FLD) -lftprintf

SOURCES_FLD = src_fdf
OBJECTS_FLD = obj_fdf
HEADERS_FLD = include_fdf
PRINT_HEAD_FLD = printf/include_printf
LIBFT_HEAD_FLD = libft
PRINT_LIB_FLD = printf
LIBFT_LIB_FLD = libft
MLX_FLD = minilibx

LIBRARIES =\
	$(LIBFT_LIB_FLD)/libft.a\
	$(PRINT_LIB_FLD)/libftprintf.a\
	$(MLX_FLD)/libmlx.a

MLX_LIB_INC = -L $(MLX_FLD) -lmlx -framework OpenGL -framework AppKit -lm

HEADERS = include_fdf/fdf.h
OBJECTS =\
	$(OBJECTS_FLD)/main.o\
	$(OBJECTS_FLD)/change_coords.o\
	$(OBJECTS_FLD)/errors.o\
	$(OBJECTS_FLD)/makes_coords.o\
	$(OBJECTS_FLD)/main_functions.o\
	$(OBJECTS_FLD)/clean_functions.o\
	$(OBJECTS_FLD)/colors.o\
	$(OBJECTS_FLD)/mlx_works.o

all : $(NAME) $(LIBRARIES)

.IGNORE : clean fclean

clean :
	rm -rf $(OBJECTS_FLD)

fclean : clean cl_w_lib
	rm -f $(NAME)

cl_w_lib :
	make fclean -C $(LIBFT_LIB_FLD)
	make fclean -C $(PRINT_LIB_FLD)
	make clean -C $(MLX_FLD)

re : fclean all

$(NAME) : $(OBJECTS) $(LIBRARIES)
	$(CC) -v $(CFLAGS) $(LIB_INCLUDE) $(OBJECTS) -o $(NAME)

$(OBJECTS_FLD)/main.o : $(SOURCES_FLD)/main.c $(HEADERS)
	@mkdir -p $(OBJECTS_FLD)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/main.c -o $(OBJECTS_FLD)/main.o

$(OBJECTS_FLD)/change_coords.o : $(SOURCES_FLD)/change_coords.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/change_coords.c -o $(OBJECTS_FLD)/change_coords.o

$(OBJECTS_FLD)/errors.o : $(SOURCES_FLD)/errors.c $(HEADERS)
	$(CC) $(INCLUDES) -I $(PRINT_HEAD_FLD) $(CFLAGS) -c $(SOURCES_FLD)/errors.c -o $(OBJECTS_FLD)/errors.o

$(OBJECTS_FLD)/makes_coords.o : $(SOURCES_FLD)/makes_coords.c $(HEADERS)
	$(CC) $(INCLUDES) -I $(PRINT_HEAD_FLD) $(CFLAGS) -c $(SOURCES_FLD)/makes_coords.c -o $(OBJECTS_FLD)/makes_coords.o

$(OBJECTS_FLD)/main_functions.o : $(SOURCES_FLD)/main_functions.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/main_functions.c -o $(OBJECTS_FLD)/main_functions.o

$(OBJECTS_FLD)/mlx_works.o : $(SOURCES_FLD)/mlx_works.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/mlx_works.c -o $(OBJECTS_FLD)/mlx_works.o

$(OBJECTS_FLD)/clean_functions.o : $(SOURCES_FLD)/clean_functions.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/clean_functions.c -o $(OBJECTS_FLD)/clean_functions.o

$(OBJECTS_FLD)/colors.o : $(SOURCES_FLD)/colors.c $(HEADERS)
	$(CC) $(INCLUDES) $(CFLAGS) -c $(SOURCES_FLD)/colors.c -o $(OBJECTS_FLD)/colors.o

$(LIBRARIES) : $(LIBFT_LIB_FLD) $(PRINT_LIB_FLD)
	make -C $(LIBFT_LIB_FLD)
	make -C $(PRINT_LIB_FLD)
	make -C $(MLX_FLD)
