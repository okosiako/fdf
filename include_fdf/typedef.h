/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typedef.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/28 10:41:51 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 11:26:04 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TYPEDEF_H
# define TYPEDEF_H

typedef const enum	e_mlx_win
{
	MLX,
	WIN
}					t_mlx_win;

typedef const enum	e_str_access
{
	X1,
	Y1,
	X2,
	Y2,
	CC
}					t_str_access;

typedef const enum	e_error_codes
{
	TOO_MANY_FILES,
	NO_FILE,
	CANT_OPEN_FILE,
	EMPTY_FILE,
	MEMORY,
	CANT_INIT_MLX,
	READ,
	COLUMS_PROBL,
	INVALID_COLOR
}					t_error_codes;

typedef struct		s_coords
{
	int			x;
	int			y;
	int			z;
	int			rout_x;
	int			rout_y;
	int			color;
}					t_coords;

typedef struct		s_temp_coords
{
	int			x1;
	int			y1;
	int			x2;
	int			y2;
}					t_temp_coords;

typedef struct		s_map_params
{
	unsigned int	step;
	int				max[2];
	int				min[2];
}					t_map_params;

#endif
