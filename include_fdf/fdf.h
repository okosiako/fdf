/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/03 16:24:40 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 13:27:14 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "libft.h"
# include "stdbool.h"
# include "typedef.h"

# define FAIL -1

# define X_SIZE 1000
# define Y_SIZE 1000

# define PI 3.141592

# define MAX(a, b) (a) > (b) ? (a) : (b)
# define MIN(a, b) (a) < (b) ? (a) : (b)

void		errors_handle(const t_error_codes error_code,
					const char *program_name);

char		*read_file(const int fd);
t_coords	**makes_coords(const char **map_by_lines,
					const size_t lines_amt);

void		do_work(char **map, const char *title);
void		rotate(t_coords **coords, t_map_params *map_param);
void		centrait(t_coords **coords, t_map_params *map_params);

int			color(char *s);

void		**create_window(const char *title);
void		put_on_window(t_coords **coord, void **mlx_win);

int			exit_x(void);
int			exit_key(int code);

/*
** clean functions
*/

void		clean_all(char ***map_by_lines, char **map,
				t_map_params **map_param, t_coords ***initial_coords);
void		clean_coords(t_coords ***coords);
void		clean_map(char ***map_by_lines);

#endif
