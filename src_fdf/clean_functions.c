/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_functions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/26 20:26:18 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 10:37:35 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	clean_map(char ***map_by_lines)
{
	size_t	count;

	count = 0;
	while ((*map_by_lines)[count] != NULL)
		ft_strdel(&((*map_by_lines)[count++]));
	ft_memdel((void **)map_by_lines);
}

void	clean_coords(t_coords ***coords)
{
	size_t	count;

	count = 0;
	while ((*coords)[count] != NULL)
	{
		ft_memdel((void **)&((*coords)[count]));
		count++;
	}
	ft_memdel((void **)coords);
}

void	clean_all(char ***map_by_lines, char **map, t_map_params **map_param,
					t_coords ***initial_coords)
{
	ft_memdel((void **)map_param);
	clean_coords(initial_coords);
	clean_map(map_by_lines);
	ft_strdel(map);
}
