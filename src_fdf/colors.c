/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/28 11:22:28 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 13:31:22 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "printf.h"

bool	valid(char c)
{
	if (c >= '0' && c <= '9')
		return (true);
	else if (c >= 'A' && c <= 'F')
		return (true);
	else if (c >= 'a' && c <= 'f')
		return (true);
	return (false);
}

int		ft_atoi_base1(char *s, int base)
{
	int		res;
	size_t	i_x;
	size_t	i_y;

	i_x = 0;
	i_y = 6;
	(s[i_x] == '0') ? i_x++ : 0;
	(s[i_x] == 'x') ? i_x++ : 0;
	while (s[i_x] && valid(s[i_x]) && i_y-- > 0)
	{
		if (s[i_x] >= '0' && s[i_x] <= '9')
			res = res * base + s[i_x] - 48;
		else if (s[i_x] >= 'A' && s[i_x] <= 'F')
			res = res * base + s[i_x] - 55;
		else if (s[i_x] >= 'a' && s[i_x] <= 'f')
			res = res * base + s[i_x] - 87;
		i_x++;
	}
	if (s[i_x])
		errors_handle(INVALID_COLOR, "fdf");
	return (res);
}

int		color(char *s)
{
	char	*buf;
	int		res;

	res = 0;
	buf = ft_strsub(s, (ft_strchr(s, ',') - s + 1),
		ft_strlen(s) - (ft_strchr(s, ',') - s) - 1);
	res = ft_atoi_base1(buf, 16);
	ft_strdel(&buf);
	return (res);
}
