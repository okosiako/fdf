/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/03 16:23:40 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 10:21:45 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <sys/stat.h>
#include <fcntl.h>

int		exit_x(void)
{
	exit(EXIT_SUCCESS);
}

int		exit_key(int code)
{
	if (code == 53)
		exit(EXIT_SUCCESS);
	return (0);
}

char	*read_file(const int fd)
{
	char	temp_buff[100];
	char	*map;
	int		len;

	map = NULL;
	while ((len = read(fd, temp_buff, 99)) > 0)
	{
		temp_buff[len] = '\0';
		map = map ? ft_strjoincanc(&map, temp_buff) : ft_strdup(temp_buff);
	}
	if (len < 0)
		errors_handle(READ, "fdf");
	return (map);
}

int		main(int ac, char *av[])
{
	char	*map;
	int		fd;

	if (ac != 2)
		errors_handle(ac == 1 ? NO_FILE : TOO_MANY_FILES, av[0]);
	if ((fd = open(av[1], O_RDONLY)) == FAIL)
		errors_handle(CANT_OPEN_FILE, av[0]);
	map = read_file(fd);
	close(fd);
	do_work(&map, av[1]);
	return (0);
}
