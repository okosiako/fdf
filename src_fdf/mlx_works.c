/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_works.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/12 19:18:00 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/26 21:28:36 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"
#include "printf.h"
#include <math.h>

void			algor(void **mlx_win, t_temp_coords coord, int color)
{
	const int	delta_x = abs(coord.x2 - coord.x1);
	const int	delta_y = abs(coord.y2 - coord.y1);
	const int	sign_x = coord.x1 < coord.x2 ? 1 : -1;
	const int	sign_y = coord.y1 < coord.y2 ? 1 : -1;
	int			error[2];

	error[0] = delta_x - delta_y;
	mlx_pixel_put(mlx_win[MLX], mlx_win[WIN], coord.x2, coord.y2, color);
	while (coord.x1 != coord.x2 || coord.y1 != coord.y2)
	{
		mlx_pixel_put(mlx_win[MLX], mlx_win[WIN], coord.x1, coord.y1, color);
		error[1] = error[0] * 2;
		if (error[1] > -delta_y)
		{
			error[0] -= delta_y;
			coord.x1 += sign_x;
		}
		if (error[1] < delta_x)
		{
			error[0] += delta_x;
			coord.y1 += sign_y;
		}
	}
}

t_temp_coords	make_str(int x1, int y1, int x2, int y2)
{
	t_temp_coords	arr;

	arr.x1 = x1;
	arr.y1 = y1;
	arr.x2 = x2;
	arr.y2 = y2;
	return (arr);
}

void			put_on_window(t_coords **coord, void **mlx_win)
{
	size_t			i_x;
	size_t			i_y;

	i_x = 0;
	while (coord[i_x] != NULL)
	{
		i_y = 0;
		while (coord[i_x][i_y].color != -1)
		{
			coord[i_x][i_y + 1].color != -1 ?
					algor(mlx_win, make_str(coord[i_x][i_y].rout_x,
											coord[i_x][i_y].rout_y,
											coord[i_x][i_y + 1].rout_x,
											coord[i_x][i_y + 1].rout_y),
					coord[i_x][i_y].color) : 0;
			coord[i_x + 1] ?
					algor(mlx_win, make_str(coord[i_x][i_y].rout_x,
											coord[i_x][i_y].rout_y,
											coord[i_x + 1][i_y].rout_x,
											coord[i_x + 1][i_y].rout_y),
					coord[i_x][i_y].color) : 0;
			i_y++;
		}
		i_x++;
	}
}

void			**create_window(const char *title)
{
	void	**mlx_win;

	mlx_win = (void **)ft_memalloc(sizeof(void *) * 2);
	mlx_win[MLX] = mlx_init();
	if (!mlx_win[MLX])
		errors_handle(CANT_INIT_MLX, "fdf");
	mlx_win[WIN] = mlx_new_window(mlx_win[MLX], X_SIZE, Y_SIZE, (char *)title);
	return (mlx_win);
}
