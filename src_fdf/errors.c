/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/03 19:17:20 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 11:28:56 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "printf.h"

void	errors_handle(const t_error_codes error_code, const char *program_name)
{
	if (error_code == TOO_MANY_FILES)
		ft_printf("%s: Too many files\n", program_name);
	else if (error_code == NO_FILE)
		ft_printf("%s: No files in input\n", program_name);
	else if (error_code == CANT_OPEN_FILE)
		ft_printf("%s: Can't open file\n", program_name);
	else if (error_code == EMPTY_FILE)
		ft_printf("%s: File is empty\n", program_name);
	else if (error_code == MEMORY)
		ft_printf("%s: Can't handle memory\n", program_name);
	else if (error_code == READ)
		ft_printf("%s: Faild to read a file\n", program_name);
	else if (error_code == COLUMS_PROBL)
		ft_printf("fdf: Not valid map(different number of elements is line)\n");
	else if (error_code == INVALID_COLOR)
		ft_printf("fdf: Not valid map(invalid colour code)\n");
	exit(EXIT_FAILURE);
}
