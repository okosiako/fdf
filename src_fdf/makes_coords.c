/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   makes_coords.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 13:06:28 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 13:27:32 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "printf.h"

/*
** line is checked before pushing to that fucntion
*/

static size_t		count_elements_number(const char **line)
{
	size_t	count;

	count = 0;
	while (line[count] != NULL)
		count++;
	return (count);
}

t_coords			*fill_struct(const char *line, int y)
{
	t_coords	*curr_line;
	char		**separ_line;
	size_t		count;

	separ_line = ft_strsplit(line, ' ');
	curr_line = (t_coords *)ft_memalloc(sizeof(t_coords) *
				(count_elements_number((const char **)separ_line) + 1));
	count = 0;
	while (separ_line[count] != NULL)
	{
		curr_line[count].x = count;
		curr_line[count].y = y;
		curr_line[count].z = ft_atoi(separ_line[count]);
		curr_line[count].color = ft_strchr(separ_line[count], ',') ?
			color(separ_line[count]) : 16777215;
		curr_line[count].rout_x = -1;
		curr_line[count].rout_y = -1;
		count++;
	}
	curr_line[count].color = -1;
	clean_map(&separ_line);
	return (curr_line);
}

t_coords			**makes_coords(const char **map_by_lines,
									const size_t lines_amt)
{
	t_coords	**coords;
	size_t		count;

	if (!(coords = (t_coords **)ft_memalloc(sizeof(t_coords *) *
											(lines_amt + 1))))
		errors_handle(MEMORY, "fdf");
	count = 0;
	while (map_by_lines[count] != NULL)
	{
		coords[count] = fill_struct(map_by_lines[count], count);
		count++;
	}
	return (coords);
}
