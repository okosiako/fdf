/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 21:06:15 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 10:38:27 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "printf.h"
#include "mlx.h"

size_t			count_number_of_lines(const char *map)
{
	size_t	line_number;
	char	*curr_position;

	if ((curr_position = ft_strchr((char *)map, '\n')))
		line_number = 1;
	else
		errors_handle(EMPTY_FILE, "fdf");
	while ((curr_position = ft_strchr(curr_position + 1, '\n')))
		line_number++;
	return (line_number + 1);
}

size_t			check_colums(t_coords **coords)
{
	size_t	i_x;
	size_t	i_y;
	int		colums;

	i_x = 0;
	colums = -1;
	while (coords[i_x] != NULL)
	{
		i_y = 0;
		while (coords[i_x][i_y].color != -1)
			i_y++;
		if (colums != -1 && i_y != (size_t)colums)
			errors_handle(COLUMS_PROBL, "fdf");
		colums = i_y;
		i_x++;
	}
	return (i_y);
}

bool			suite(t_map_params *map)
{
	if (map->step == 11 || map->min[X1] < 0 || map->min[Y1] < 0
	|| map->max[X1] > X_SIZE ||
	map->max[Y1] > Y_SIZE)
	{
		(map->step)--;
		return (false);
	}
	return (true);
}

t_map_params	*create_params(void)
{
	t_map_params	*map_params;

	map_params = ft_memalloc(sizeof(t_map_params));
	map_params->step = 11;
	return (map_params);
}

void			do_work(char **map, const char *title)
{
	char			**map_by_lines;
	t_coords		**initial_coords;
	void			**mlx_win;
	t_map_params	*map_param;

	map_by_lines = ft_strsplit(*map, '\n');
	map_param = create_params();
	initial_coords =
		makes_coords((const char **)map_by_lines, count_number_of_lines(*map));
	check_colums(initial_coords);
	while (map_param->step != 2 && !suite(map_param))
		rotate(initial_coords, map_param);
	mlx_win = create_window(title);
	put_on_window(initial_coords, mlx_win);
	mlx_hook(mlx_win[WIN], 17, 1L << 17, exit_x, mlx_win[MLX]);
	mlx_key_hook(mlx_win[WIN], exit_key, 0);
	mlx_loop(mlx_win[MLX]);
	clean_all(&map_by_lines, map, &map_param, &initial_coords);
}
