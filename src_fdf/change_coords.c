/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_coords.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/26 18:45:20 by okosiako          #+#    #+#             */
/*   Updated: 2017/06/28 13:23:56 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "math.h"

static void	init_max_min(t_map_params *map_param)
{
	map_param->max[X1] = -2147483648;
	map_param->max[Y1] = -2147483648;
	map_param->min[X1] = 2147483647;
	map_param->min[Y1] = 2147483647;
}

static void	max_min(t_map_params *map_param, int x, int y)
{
	map_param->max[X1] = MAX(map_param->max[X1], x);
	map_param->min[X1] = MIN(map_param->min[X1], x);
	map_param->max[Y1] = MAX(map_param->max[Y1], y);
	map_param->min[Y1] = MIN(map_param->min[Y1], y);
}

void		count_new_coords(int *x, int *y, int w_coords[3])
{
	const double	a = -30 * PI / 180.0;
	const double	b = -15 * PI / 180.0;

	*x = cos(b) * w_coords[0] + (-sin(b)) * w_coords[2];
	*y = sin(a) * sin(b) * w_coords[0] + cos(a) *
		w_coords[1] + sin(a) * cos(b) * w_coords[2];
}

void		centrait(t_coords **coords, t_map_params *map_params)
{
	size_t	i_x;
	size_t	i_y;
	int		board_x;
	int		board_y;

	board_x = abs((X_SIZE - (map_params->max[X1] - map_params->min[X1])) / 2);
	board_y = abs((Y_SIZE - (map_params->max[Y1] - map_params->min[Y1])) / 2);
	i_x = 0;
	while (coords[i_x] != NULL)
	{
		i_y = 0;
		while (coords[i_x][i_y].color != -1)
		{
			coords[i_x][i_y].rout_x += board_x;
			coords[i_x][i_y].rout_y += board_y;
			i_y++;
		}
		i_x++;
	}
	map_params->max[X1] += board_x;
	map_params->min[X1] += board_x;
	map_params->max[Y1] += board_y;
	map_params->min[Y1] += board_y;
}

void		rotate(t_coords **xyz, t_map_params *map_param)
{
	size_t			i_x;
	size_t			i_y;
	int				w_coords[3];

	i_x = 0;
	init_max_min(map_param);
	while (xyz[i_x] != NULL)
	{
		i_y = 0;
		while (xyz[i_x][i_y].color != -1)
		{
			w_coords[0] = xyz[i_x][i_y].x * map_param->step;
			w_coords[1] = xyz[i_x][i_y].y * map_param->step;
			w_coords[2] = xyz[i_x][i_y].z * map_param->step;
			count_new_coords(&(xyz[i_x][i_y].rout_x), &(xyz[i_x][i_y].rout_y),
								w_coords);
			max_min(map_param, xyz[i_x][i_y].rout_x, xyz[i_x][i_y].rout_y);
			i_y++;
		}
		i_x++;
	}
	centrait(xyz, map_param);
}
