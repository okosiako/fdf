# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This project is about creating a simplified graphic "wireframe" representation of a relief landscape linking various points (x, y, z) via segments. The coordinates of this landscape are stored in a file passed as a parameter to the program.

![Screen Shot 2017-06-30 at 6.06.32 PM.png](https://bitbucket.org/repo/Mrn7k6G/images/3076068369-Screen%20Shot%202017-06-30%20at%206.06.32%20PM.png) "example of file with coordinates"

Each number corresponds to a point in space:

* The horizontal position corresponds to its axis.
* The vertical position corresponds to its ordinate.
* The value corresponds to its altitude.

Graphic representation:

+ Type of projection: iso;
+ It's able to quit the program by pressing 'esc' or 'X' on window;
+ Map is sized to the most size to fit in window

### How do I get set up? ###

* Graphic library: miniLibx;
* Other libraries: ft_libft, ft_printf;
* To run program you need write make in folder and run exe file with ONE file as parametr (examples of maps you can find in 'test_maps' folder);
* Invalid map has:

    + different elements in lines;
    + empty file.

![Screen Shot 2017-06-30 at 6.09.09 PM.png](https://bitbucket.org/repo/Mrn7k6G/images/2626246840-Screen%20Shot%202017-06-30%20at%206.09.09%20PM.png) "mars.fdf"